//  Import data layer.
var db = require("mongoose");
db.connect("mongodb://localhost/push");

//  Import the authorization module.
var Auth = require("./module/Auth")(db);

//  Import the socket.io library.
var io = require("socket.io").listen(1337);

//  Import the channel module.
var Channel = require("./module/Channel")(db);

//  Define connection handler.
io.sockets.on("connection", function(socket) {
    //  Identify the socket.
    Auth.identifySocket(socket);
    
    socket.on("authorize", function(data) {
        socket.get("uuid", function(e, d) {
            console.log(d + " wants to authorize");
        });
        
        var apiKey = data.key;
        Auth.authorizeKey(apiKey, socket, function() { });
    });
    
    socket.on("subscribe", function(data) {
        Channel.subscribe(socket, data.channel);
    });
    
    socket.on("push", function(data) {
        console.log("Pushing data...");
        Channel.push(socket, data);
    });
    
    /**
     * Socket disconnected.
    */
    socket.on("disconnect", function() {
        socket.get("uuid", function(err, d) {
            console.log(d + " disconnected");
            Channel.unsubscribeAll(socket);
        });
    });
});