module.exports = function(db) {    
    var channels = {};
        
    var subscribeToChannel = function(socket, channel) {
        socket.get("key", function(err, data) {
            if (!data) {
                socket.emit("subscribeResult", { 
                    status: 0,
                    channel: channel
                });
                return;
            }
            socket.emit("subscribeResult", { 
                status : 1, 
                channel: channel
            });
            
            if (!channels[data]) {
                channels[data] = {};
            }
            if (!channels[data][channel]) {
                channels[data][channel] = [];
            }
            socket.get("uuid", function(err, id) {
                if (id) {
                    channels[data][channel].push(socket);
                }
            });
        });
    };
    
    var unsubscribeFromChannel = function(socket, channel) {
        socket.get("key", function(err, key) {
            if ((key) && (channels[key]) && (channels[key][channel])) {
                socket.get("uuid", function(err2, id) {
                    if (!id) {
                        return;
                    }
                    var i = channels[key][channel].indexOf(socket);
                    if (i != -1) {
                        channels[key][channel].splice(i, 1);
                    }
                });
            }
        });
    };
    
    var unsubscribeFromChannels = function(socket) {
        socket.get("key", function(err, key) {
            if ((key) && (channels[key])) {
                socket.get("uuid", function(err2, id) {
                    if (!id) {
                        return;
                    }
                    for (var c in channels[key]) {
                        var i = channels[key][c].indexOf(socket);
                        if (i != -1) {
                            channels[key][c].splice(i, 1);
                        }
                    }
                });
            }
        });
    };
    
    var pushToChannel = function(socket, data) {
        socket.get("key", function(err, key) {
            var channelName = data.channel;
            if ((key) && (channels[key]) && (channels[key][channelName])) {
                var pushData = data.data;
                socket.get("uuid", function(err2, id) {
                    if (!id) {
                        return;
                    }
                    var i = channels[key][channelName].indexOf(socket);
                    if (i != -1) {
                        for (var j = 0; j < channels[key][channelName].length; j++) {
                            if (j == i) {
                                continue;
                            }
                            var s = channels[key][channelName][j];
                            s.emit(channelName, pushData);
                        }
                    }
                });
            }
        });
    };
    
    return {
        subscribe: subscribeToChannel,
        unsubscribe: unsubscribeFromChannel,
        unsubscribeAll: unsubscribeFromChannels,
        push: pushToChannel
    };
};