module.exports = function(db) {
    //
    //  Prepare database stuff.
    //
    var ApiKeySchema = new db.Schema({
        clientName: { type: String },
        apiKey:     { type: String }
    });
    
    var ApiKey = db.model("ApiKey", ApiKeySchema);
       
    /**
     * UUID generator.
     *
     * @see http://stackoverflow.com/a/7061193/759049
    */
    var UUIDv4 = function b(a){return a?(a^Math.random()*16>>a/4).toString(16):
        ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g,b)};
    
    /**
     * Authorizes the given key.
    */
    var authorizeKey = function(key, socket, callback) {
        ApiKey.findOne({
            apiKey: key
        }, function(err, doc) {
            if (doc != null) {
                socket.emit("authorizeResult", { status: 1 });
                socket.set("key", doc._id, function() { });
                
                socket.get("uuid", function(err, data) {
                    console.log(data + " authorized");
                });
            } else {
                socket.emit("authorizeResult", { status: 0 });
            }
        });
    };
    
    /**
     * Assigns the given socket a UUID.
    */
    var identifySocket = function(socket) {
        socket.set("uuid", UUIDv4(), function() {
            socket.get("uuid", function(x, n) {
                console.log(n + " connected");
                socket.emit("identify", { id: n });
            });
        });
    };
    
    return {
        authorizeKey: authorizeKey,
        identifySocket: identifySocket
    };
};