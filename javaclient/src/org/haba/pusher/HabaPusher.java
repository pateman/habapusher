package org.haba.pusher;

import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Main HabaPusher class.
 * 
 * This class allows only to push notifications to a channel. It's not meant
 * for receiving data. 
 * 
 * @author Patryk Nusbaum <patryk@nusbaum.pl>
 */
public final class HabaPusher {
    private final SocketIO handle;
    private String _id;
    private String apiKey;
    private Boolean _authorized = null;
    private final List<String> subscribedChannels = new ArrayList<>();
    
    class EventQueue {
        private Map<String, Map<String, Object>> events = new HashMap<>();
        private final Timer timer = new Timer();
        
        class QueueTask extends TimerTask {
            private final EventQueue q;
            
            public QueueTask(EventQueue queue) {
                this.q = queue;
            }
            
            @Override
            public void run() {
                this.q.flush();
            }
            
        }
        
        public void pushEvent(String eventName, Map<String, Object> data) {
            this.events.put(eventName, data);
            this.flush();
        }
        
        public void flush() {
            if (_authorized == null) {
                this.timer.schedule(new QueueTask(this), 100);
                return;
            }
            
            if (_authorized) {
                for (Entry<String, Map<String, Object>> e : this.events.
                    entrySet()) {
                    final Map<String, Object> v = e.getValue();
                    if (v == null) {
                        continue;
                    }
                    handle.emit(e.getKey(), v);
                }
                this.events.clear();
            }
        }
    }
    private final EventQueue queue = new EventQueue();    
    
    public HabaPusher(String serverURI, 
        final String apiKey) throws MalformedURLException {
        this.apiKey = apiKey;
        
        this.handle = new SocketIO(serverURI);
        this.handle.connect(new IOCallback() { 

            @Override
            public void onDisconnect() {
                
            }

            @Override
            public void onConnect() {

            }

            @Override
            public void onMessage(String string, IOAcknowledge ioa) {
                
            }

            @Override
            public void onMessage(JSONObject jsono, IOAcknowledge ioa) {
                
            }

            @Override
            public void on(String string, IOAcknowledge ioa, Object... os) {
                Integer status;
                final JSONObject o = ((JSONObject)os[0]);
                
                if (string.equalsIgnoreCase("identify")) {
                    try {
                        _id = o.getString("id");
                    } catch (JSONException ex) {
                        _id = null;
                    }
                    
                    if (_id != null) {
                        Map<String, String> m = new HashMap<>();
                        m.put("key", apiKey);
                        handle.emit("authorize", m);
                    }
                } else if (string.equalsIgnoreCase("authorizeResult")) {
                    try {
                        status = o.getInt("status");
                    } catch (JSONException ex) {
                        status = null;
                    }
                    
                    _authorized = ((status != null) && (status != 0));
                } else if (string.equalsIgnoreCase("subscribeResult")) {
                    String channel;
                    try {
                        status = o.getInt("status");
                        channel = o.getString("channel");
                    } catch (JSONException ex) {
                        status = null;
                        channel = null;
                    }
                    
                    if ((status != null) && (status == 1)) {
                        subscribedChannels.add(channel);
                    }
                }
            }

            @Override
            public void onError(SocketIOException sioe) {
                
            }
            
        });
    }
    
    public void subscribe(String channelName) {
        final Map<String, Object> d = new HashMap<>();
        d.put("channel", channelName);
        this.queue.pushEvent("subscribe", d);
    }
    
    public void push(String channelName, Map<String, Object> data) {
        final Map<String, Object> d = new HashMap<>();
        d.put("channel", channelName);
        d.put("data", data);
        this.queue.pushEvent("push", d);
    }

    public String getId() {
        return _id;
    }

    public boolean isAuthorized() {
        return _authorized;
    }

    public String getApiKey() {
        return apiKey;
    }

    public List<String> getSubscribedChannels() {
        return subscribedChannels;
    }
    
    public boolean isSubscribedToChannel(String channelName) {
        return (subscribedChannels.indexOf(channelName) != -1);
    }
}
