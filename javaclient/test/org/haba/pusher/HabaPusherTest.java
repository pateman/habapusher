package org.haba.pusher;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests.
 * 
 * @author Patryk Nusbaum <patryk@nusbaum.pl>
 */
public class HabaPusherTest {
    
    public HabaPusherTest() {
    }
   
    @Test
    public void subscribeTest() throws MalformedURLException, 
        InterruptedException {
        final HabaPusher hp = new HabaPusher("http://localhost:1337", 
            "qazwsx12");
        hp.subscribe("test");
        
        Thread.sleep(5000);
        assertTrue(hp.isSubscribedToChannel("test"));
    }
    
    @Test
    public void pushTest() throws MalformedURLException, InterruptedException {
        final HabaPusher hp = new HabaPusher("http://localhost:1337", 
            "qazwsx12");
        hp.subscribe("test");
        
        final Map<String, Object> d = new HashMap<>();
        d.put("sender", hp.getId());
        d.put("msg", "Java client test");
        Thread.sleep(5000);
        hp.push("test", d);
    }
}