/**
 * habaPusher.
*/
function habaPusher(host, apiKey) {
    var socket = io.connect(host);
    
    var _id = null;
    var _authorized = null;
    var subscribedChannels = [];
    var onData = {};
    
    /**
     * Basic event queue.
    */
    var EventQueue = function() {
        var _events = {};
        var interval;
        
        /**
         * Taken from jQuery.
        */
        var isEmptyObject = function (obj) {
            var name;
            for (name in obj) {
                return false;
            }
            return true;
        };
        
        this.pushEvent = function(eventName, eventData) {
            _events[eventName] = eventData;
            this.flush();
        };
        
        this.flush = function() {
            if (_authorized === null) {
                if (!interval) {
                    interval = setInterval(this.flush, 100);
                }
                return;
            }
            
            clearInterval(interval);
            if ((_authorized) && (!isEmptyObject(_events))) {
                for (var k in _events) {
                    socket.emit(k, _events[k]);
                }
                _events = {};
            }
        };
    };
    var queue = new EventQueue();
    
    socket.on("identify", function(data) {
        console.log("Identified as " + data.id);
        _id = data.id;
        
        socket.emit("authorize", { key: apiKey });
    });
    
    socket.on("authorizeResult", function(data) {
        if (data.status == 0) {
            console.error("Invalid API key.");
            _authorized = false;
        } else {
            console.log("Authorization OK.");
            _authorized = true;
        }
    });
    
    socket.on("subscribeResult", function(data) {
        if (data.status == 1) {
            subscribedChannels.push(data.channel);
            console.log("Subscription OK.");

            socket.on(data.channel, function(d) {
                if (onData[data.channel]) {
                    onData[data.channel](d);
                }
            });
        } else {
            console.error("Subscription failed.");
            delete onData[data.channel];
        }
    });
    
    /**
     * Returns the client's identifier.
    */
    this.getId = function() {
        return _id;
    };
    /**
     * Subscribes to the given channel.
    */
    this.subscribe = function(channelName, callback) {
        onData[channelName] = callback;
        queue.pushEvent("subscribe", { channel: channelName });
    };
    this.push = function(channelName, pushData) {
        queue.pushEvent("push", { channel: channelName, data: pushData });
    };
};